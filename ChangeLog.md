## v0.1.0

- using asyncio to manage concurrent calls to `dspsr`

## v0.1.1

- Using `yaml.load(input, loader=yaml.SafeLoader)` instead of bare
`yaml.load(input)` call
- Added some logging messages to test methods
- Fixed bug where tests wouldn't run if the specified name didn't begin
with `test_`
- output archives and text files are prepended with the name of the respective
test

## v0.2.0

- Output files go to `products` subdirectory instead of base directory.
- Using TOML instead of YAML for test configuration.
- Changed expected order in test configuration to test method name, dspsr parameters, test file path.
- Default log level for partialize is ERROR, even in verbose mode.

## v0.3.0

- refactored utility code into `dspsr_test` subdirectory.
- fixed set_dspsr_executables.py

## v0.4.0

- renamed test.config.toml to verify.config.toml
- added unit tests in `test/`
- added `follow` function that just runs a bunch of coroutines
in sequence.
- added `run_dspsr_with_dump` function.

## v0.4.1

- uses DSPSR_TEST_DATA_DIR to find test data files

## v0.5.0

- no longer using Python's unittest to generate test cases. Instead, I'm
generating cases by hand and running them with tqdm for a nice loading bar.
Alternatively, tests can be run all at once using `asyncio.gather`. This might
be marginally fast than running them in sequence.
- Adding verify/run_dspsr that just runs a single build of dspsr against a
bunch of test cases. This doesn't do any checking to see if anything is
correct, rather it just runs code and generates archives.
- verify/run_dspsr can run the same dspsr command against the same file with
different parameters, ultimately comparing the output archives.
- verify/run_dspsr automatically creates PNG plots of DSPSR runs.
- Added dspsr_test/common.py which defines a set of a common configuration and
running tools for creating verification tests. This also defines an interface
by which to create tests.
- verify/run_dspsr.py can run with two (lists of) inputs with a single command
line option. This will psrdiff the archives of each run.
- verify/run_dspsr.py can run with two (lists of) inputs with a set of command
line options. This will run dspsr with each set of inputs and each set of
command line options. This will psrdiff the archives of each run.

## v0.6.0

- the `util.run_dspsr` function can now be used to handle DSPSR calls that
create multiple output files.
- added some DSPSR API scripts in `sandbox/`

## v0.6.1

- Fixed issue with `util.run_dspsr` where it would not behave correctly with
single archive output files.
- Fixed issue where API tests were tied to a specific dataset revision.

## v0.7.0

- Added Dockerfile that allows for running `get_gDMCP_data.py` script in a docker container.
- Added automatic docker deploy in `gitlab-ci.yml`. 
